TEAR-OFF METHODOLOGY

Methodology used when tearing the Arduino library environment off from the Arduino build environment. This document is only needed if you are going to create your own tearduino environment, such as for example when creating a tearduino for your own MCU. If you use 

PREREQUISITES

You need to be knowledgeable in GNU make, in compiling and linking with the GNU compiler collection, as well as linking and building library archives. So the following tools needs to be recognized:
 * gcc
 * g++
 * ar
 * make

In order to tear off, you need to install the Arduino IDE and install the relevant development card of your choice. 

You also need to have your system PATH point out the directory where the avr-gcc, avrdude and similar tools are stored. On some Linux distros the avr-gcc (or gcc-avr) tools are installable from the standard repository, and then they are installed in the standard PATH. You may use that, you may use the Arduino execs, or you may download and install from Internet (f.ex. http://blog.zakkemble.net/avr-gcc-builds/). If you use Arduino execs, you need to find them in your installation - in Windows open the explorer and search for avr-gcc.exe, see that the file path to that exe is added to PATH through the Control Panel - in Linux you open a terminal and search from your home directory through
  
  find ~ -name avr-gcc

In the path you get, remove the last /avr-gcc, edit ~/.bashrc and add

  export PATH=$PATH:the-path-you-got

PREPARATIONS

The Arduino IDE is used as the tool to determine how to create the Tearduino environment. Preferences must be set to be very verbose:

  File > Preferences opens the Preferences dialogue:

  In the dialogue, in 'Show verbose output during:' the two boxes 'compilation' and 'upload' must be checked on.

  'Compiler warnings:' must be set to 'More' or 'All'.

GENERAL METHODOLOGY

When you rip off, you are trying to implement the behavior of the output in the "console area" of the sketch window. It is therefore necessary to put on all console output according to the previous section PREPARATIONS to put all necessary logging on.

When you tear off 'Tearduino' from the Arduino build environment, you export the examples from the Arduino to your own example directory. 

Starting with the example File > Examples > 01.Basics > Blink, you open the example and press the Verify button (the ring containing √) to make the code generation start. When the code generation output in the console area stops with something like

  Sketch uses 930 bytes (2%) of program storage space. [...]
  Global variables use 9 bytes (0%) of dynamic memory, [...]

you marks all content in the console area and press Ctrl-A Ctrl-C (Church-A Church-C on Mac) to select all and copy all its content to the clipboard. Then you paste all the text into your very proficient text editor of your choice for analysis (I use kate on Linux, Notepad++ on Windows have a good reputation of course, but real sharks of course use vim or something associated with a cult).

The console log (compilation log copied from the console area) is a horrible spaghetti monster with full of opaque file paths (the noodles), and the tricky part is to sift out the relevant commands (the meatballs). If you are systematic, you set up make-styled variables in your log analysis file and replace the paths with variable addresses. For example I analyse a Uno compilation, and when substituting the first line in my Blink compilation I create the following variables:

  AENV    = /opt/part3/rursus/homerursus/Programs/arduino/arduino-1.8.7
  HWENV   = $(AENV)/hardware
  AVR     = $(TOOLS)/avr

and compress the first output line to 

  $(AENV)/arduino-builder -dump-prefs -logger=machine -hardware  $(HWENV) -hardware /home/rursus/.arduino15/packages -tools $(AENV)/tools-builder -tools $(AVR) -tools /home/rursus/.arduino15/packages -built-in-libraries $(AENV)/libraries -libraries /home/rursus/Arduino/libraries -fqbn=arduino:avr:uno -ide-version=10807 -build-path /tmp/arduino_build_604379 -warnings=more -build-cache /tmp/arduino_cache_117394 -prefs=build.warn_data_percentage=75 -prefs=runtime.tools.arduinoOTA.path=$(AVR) -prefs=runtime.tools.arduinoOTA-1.2.1.path=$(AVR) -prefs=runtime.tools.avrdude.path=$(AVR) -prefs=runtime.tools.avrdude-6.3.0-arduino14.path=$(AVR) -prefs=runtime.tools.avr-gcc.path=$(AVR) -prefs=runtime.tools.avr-gcc-5.4.0-atmel3.6.1-arduino2.path=$(AVR) -verbose $(AENV)/examples/01.Basics/Blink/Blink.ino

Here we experience that the meatball is arduino-builder, which is what we are trying to eliminate in the development process. The log output contains two 'arduino-builder' calls, that we pass by.

After the two arduino-builder calls there is a avr-g++ compilation of Blink.ino.cpp. Blink.ino.cpp is what we're after, we make a catalog examples/01_basics/blink, and we copy 
Blink.ino.cpp to examples/01_basics/blink/blink.cpp, and create a Makefile in examples/01_basics/blink

...[TO BE CONTINUED]...
